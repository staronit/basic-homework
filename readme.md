## Basic homework

### Concept
It's a domain driven structure of small app with a minimal frontend
and api section. Data stream structure:
- user enters controller
- controllers try to get domain objects from repository
- repository call data providers (database, api in this specific order) for DTO objects
- DTO objects are mapped to domains and returned to controller for further buissnes logic
- in API data are mapped to View Object and them serialized to JSON

## Usage

- fill trello api member, apikey and token in .env file
- composer install