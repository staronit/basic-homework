<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BoardController@boards')
    ->name('app.board.index');

Route::get('/{boardId}', 'ColumnController@columns')
    ->where('boardId', '[A-Za-z0-9]+')
    ->name('app.board.detail');