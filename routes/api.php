<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/boards', 'BoardApiController@boards');

Route::get('/boards/{boardId}/columns', 'ColumnApiController@columns')
    ->where('boardId', '[A-Za-z0-9]+');

