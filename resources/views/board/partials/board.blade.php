<li class="board">
    <a href="{{ route('app.board.detail', [
        'boardId' => $board->getId()
    ]) }}">{{ $board->getName() }}</a>
</li>