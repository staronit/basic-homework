@extends('layouts.base')

@section('title', 'Boards')

@section('content')
    <h2>List of boards ({{ count($boards) }})</h2>
    <ul>
    @foreach ($boards as $board)
        @include('board.partials.board', ['board' => $board])
    @endforeach
    </ul>
@endsection