<div class="column">
    <h3>{{ $column->getName() }} ({{ $column->getCardsCount() }})</h3>
    <ul>
    @foreach ($column->getCards() as $card)
        @include ('columns.partial.card', ['card' => $card])
    @endforeach
    </ul>
</div>