@extends('layouts.base')

@section('title', 'Columns')

@section('content')
    @foreach ($columns as $column)
        @include('columns.partial.column', ['column' => $column])
    @endforeach
@endsection