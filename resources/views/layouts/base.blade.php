<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Homework demo - @yield('title')</title>
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>

<div class="container">
    <header>
        <h1>HomeworkApp</h1>
    </header>
    <div class="content">
        @yield('content')
    </div>
    <footer>
        <h3>Assumptions</h3>
        <p>
            Because it's a test task, and in the scenario was written to make assumptions
            if something is not clear, a make few of them:
        </p>
        <ol>
            <li>skip authorization</li>
            <li>database is used as "cache"</li>
            <li>poor error handling</li>
            <li>due time problems unfortunately i had to skip unit test</li>
        </ol>
        <h3>API</h3>
        <code>
            /api/boards
        </code>
        <p>returns list of boards</p>
        <code>
            /api/boards{id}/columns
        </code>
        <p>returns list of columns with cards</p>
    </footer>
</div>
</body>
</html>