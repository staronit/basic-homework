<?php

namespace App\DTO\Card\CardDTOFactoryInterface;


use App\DTO\Card\CardDTO;

class CardDTOApiFactory implements CardDTOFactoryInterface
{
    const FIELD_ID = 'id';
    const FIELD_NAME = 'name';

    /**
     * @inheritDoc
     */
    public function create(array $data): CardDTO
    {
        return new CardDTO(
            $data[self::FIELD_ID],
            $data[self::FIELD_NAME]
        );
    }

    /**
     * @inheritDoc
     */
    public function createCollection(array $data): array
    {
        $cards = [];
        foreach ($data as $cardData) {
            $cards[] = $this->create($cardData);
        }

        return $cards;
    }

}