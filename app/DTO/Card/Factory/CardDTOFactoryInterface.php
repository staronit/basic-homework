<?php

namespace App\DTO\Card\CardDTOFactoryInterface;


use App\DTO\Card\CardDTO;

interface CardDTOFactoryInterface
{
    /**
     * @param array $data
     * @return CardDTO
     */
    public function create(array $data): CardDTO;

    /**
     * @param array $data
     * @return CardDTO[]
     */
    public function createCollection(array $data): array;
}