<?php

namespace App\DTO\Card;

class CardDTO
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var
     */
    private $description;

    /**
     * CardDTO constructor.
     * @param string $id
     * @param $description
     */
    public function __construct($id, $description)
    {
        $this->id = $id;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

}