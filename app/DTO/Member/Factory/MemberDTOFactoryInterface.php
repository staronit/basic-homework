<?php

namespace App\DTO\Member\Factory;


use App\DTO\Member\MemberDTO;

interface MemberDTOFactoryInterface
{
    /**
     * @param array $data
     * @return MemberDTO
     */
    public function create(array $data): MemberDTO;

    /**
     * @param array $data
     * @return MemberDTO[]
     */
    public function createCollection(array $data): array;
}