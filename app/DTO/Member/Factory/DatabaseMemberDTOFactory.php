<?php

namespace App\DTO\Member\Factory;

use App\DTO\Member\MemberDTO;

class DatabaseMemberDTOFactory implements MemberDTOFactoryInterface
{
    const FIELD_ID = 'id';
    const FIELD_NAME = 'name';
    const FIELD_EMAIL = 'email';

    /**
     * @inheritdoc
     */
    public function create(array $data): MemberDTO
    {
        return new MemberDTO(
            $data[self::FIELD_ID],
            $data[self::FIELD_NAME],
            $data[self::FIELD_EMAIL]
        );
    }

    /**
     * @inheritdoc
     */
    public function createCollection(array $data): array
    {
        $boards = [];
        foreach ($data as $boardData) {
            $boards[] = $this->create($boardData);
        }

        return $boards;
    }

}