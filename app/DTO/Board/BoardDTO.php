<?php

namespace App\DTO\Board;


use App\DTO\Member\MemberDTO;

class BoardDTO
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var MemberDTO[]
     */
    private $members;

    /**
     * BoardDTO constructor.
     * @param string $id
     * @param string $name
     * @param MemberDTO[] $members
     */
    public function __construct($id, $name, array $members)
    {
        $this->id = $id;
        $this->name = $name;
        $this->members = $members;
    }


    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return MemberDTO[]
     */
    public function getMembers(): array
    {
        return $this->members;
    }

}