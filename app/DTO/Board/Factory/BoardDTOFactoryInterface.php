<?php

namespace App\DTO\Board\Factory;


use App\DTO\Board\BoardDTO;

interface BoardDTOFactoryInterface
{
    /**
     * @param array $data
     * @param array $members
     * @return BoardDTO
     */
    public function create(array $data, array $members): BoardDTO;

    /**
     * @param array $data
     * @param array $members
     * @return BoardDTO[]
     */
    public function createCollection(array $data, array $members): array;
}