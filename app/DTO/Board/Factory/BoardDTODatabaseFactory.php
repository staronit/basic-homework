<?php

namespace App\DTO\Board\Factory;

use App\DTO\Board\BoardDTO;
use App\DTO\Member\Factory\MemberDTOFactoryInterface;
use App\DTO\Member\MemberDTO;

class BoardDTODatabaseFactory implements BoardDTOFactoryInterface
{
    const FIELD_ID = 'id';
    const FIELD_NAME = 'name';
    const FIELD_MEMBERSHIP = 'memberships';

    /**
     * @var MemberDTOFactoryInterface
     */
    private $memberDTOFactory;

    /**
     * BoardDTOFactory constructor.
     * @param MemberDTOFactoryInterface $memberDTOFactory
     */
    public function __construct(MemberDTOFactoryInterface $memberDTOFactory)
    {
        $this->memberDTOFactory = $memberDTOFactory;
    }

    /**
     * @inheritdoc
     */
    public function create(array $data, array $members): BoardDTO
    {

        return new BoardDTO(
            $data[self::FIELD_ID],
            $data[self::FIELD_NAME],
            $this->createMembers($data[self::FIELD_MEMBERSHIP])
        );
    }

    /**
     * @inheritdoc
     */
    public function createCollection(array $data, array $members): array
    {
        $boards = [];
        foreach ($data as $boardData) {
            $boards[] = $this->create($boardData, $members);
        }

        return $boards;
    }

    /**
     * @param array $membership
     * @return MemberDTO[]
     */
    private function createMembers(array $membership): array
    {
        $membersDTO = [];
        foreach ($membership as $member) {
            $membersDTO[] = $this->memberDTOFactory->create($member);
        }

        return $membersDTO;

    }

}