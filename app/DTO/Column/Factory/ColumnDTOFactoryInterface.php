<?php

namespace App\DTO\Column\Factory;


use App\DTO\Column\ColumnDTO;

interface ColumnDTOFactoryInterface
{
    /**
     * @param array $data
     * @return ColumnDTO
     */
    public function create(array $data): ColumnDTO;

    /**
     * @param array $data
     * @return ColumnDTO[]
     */
    public function createCollection(array $data): array;
}