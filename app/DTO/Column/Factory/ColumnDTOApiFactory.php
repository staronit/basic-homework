<?php


namespace App\DTO\Column\Factory;


use App\DTO\Card\CardDTOFactoryInterface\CardDTOFactoryInterface;
use App\DTO\Column\ColumnDTO;

class ColumnDTOApiFactory implements ColumnDTOFactoryInterface
{
    const FIELD_ID = 'id';
    const FIELD_NAME = 'name';
    const FIELD_BOARD_ID = 'idBoard';
    const FIELD_CARDS = 'cards';

    /**
     * @var CardDTOFactoryInterface
     */
    private $cardDTOFactory;

    /**
     * ColumnDTOApiFactory constructor.
     * @param CardDTOFactoryInterface $cardDTOFactory
     */
    public function __construct(CardDTOFactoryInterface $cardDTOFactory)
    {
        $this->cardDTOFactory = $cardDTOFactory;
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): ColumnDTO
    {
        return new ColumnDTO(
            $data[self::FIELD_ID],
            $data[self::FIELD_NAME],
            $data[self::FIELD_BOARD_ID],
            $this->cardDTOFactory->createCollection($data[self::FIELD_CARDS])
        );
    }

    /**
     * @inheritDoc
     */
    public function createCollection(array $data): array
    {
        $columns = [];
        foreach ($data as $columnData) {
            $columns[] = $this->create($columnData);
        }

        return $columns;
    }

}