<?php

namespace App\DTO\Column;


use App\DTO\Card\CardDTO;

class ColumnDTO
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $boardId;

    /**
     * @var CardDTO[]
     */
    private $cards;

    /**
     * ColumnDTO constructor.
     * @param string $id
     * @param string $name
     * @param string $boardId
     * @param CardDTO[] $cards
     */
    public function __construct($id, $name, $boardId, array $cards)
    {
        $this->id = $id;
        $this->name = $name;
        $this->boardId = $boardId;
        $this->cards = $cards;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getBoardId(): string
    {
        return $this->boardId;
    }

    /**
     * @return CardDTO[]
     */
    public function getCards(): array
    {
        return $this->cards;
    }
}