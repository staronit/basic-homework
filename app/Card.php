<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $table = 'cards';

    protected $fillable = [
        'id', 'description',
    ];

    public $incrementing = false;

    public function column()
    {
        return $this->belongsTo('App\Column');
    }
}
