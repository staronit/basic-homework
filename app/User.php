<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email'
    ];

    public $incrementing = false;


    public function boards()
    {
        return $this->belongsToMany('App\Board', 'user_board');
    }


}
