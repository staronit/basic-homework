<?php


namespace App\DataProvider\Column;


use App\DataProvider\Column\Exception\ColumnsNotFoundException;
use App\DataProvider\Column\Result\ColumnsDataProviderResultAbstract;

interface ColumnDataProviderInterface
{
    /**
     * @param string $boardId
     * @return ColumnsDataProviderResultAbstract
     * @throws ColumnsNotFoundException
     */
    public function getColumns(string $boardId): ColumnsDataProviderResultAbstract;
}