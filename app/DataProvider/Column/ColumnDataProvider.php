<?php


namespace App\DataProvider\Column;


use App\DataProvider\Column\Exception\ColumnsNotFoundException;
use App\DataProvider\Column\Providers\ColumnProviderInterface;
use App\DataProvider\Column\Result\ColumnsDataProviderResultAbstract;

class ColumnDataProvider implements ColumnDataProviderInterface
{
    /**
     * @var ColumnProviderInterface[]
     */
    private $providers;

    /**
     * ColumnDataProvider constructor.
     * @param ColumnProviderInterface[] $providers
     */
    public function __construct(array $providers)
    {
        $this->providers = $providers;
    }

    /**
     * @inheritdoc
     */
    public function getColumns(string $boardId): ColumnsDataProviderResultAbstract
    {
        reset($this->providers);
        foreach ($this->providers as $dataProvider) {
            try {
                return $dataProvider->getColumns($boardId);
            } catch (ColumnsNotFoundException $exception) {
            }
        }

        throw new ColumnsNotFoundException();
    }

}