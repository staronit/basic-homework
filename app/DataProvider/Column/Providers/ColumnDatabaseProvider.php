<?php


namespace App\DataProvider\Column\Providers;

use App\Column;
use App\DataProvider\Column\Exception\ColumnsNotFoundException;
use App\DataProvider\Column\Result\ColumnsDataProviderDatabaseResult;
use App\DataProvider\Column\Result\ColumnsDataProviderResultAbstract;
use App\DTO\Column\Factory\ColumnDTODatabaseFactory;
use App\DTO\Column\Factory\ColumnDTOFactoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ColumnDatabaseProvider implements ColumnProviderInterface
{
    /**
     * @var ColumnDTOFactoryInterface
     */
    private $columnDTOFactory;

    /**
     * ColumnDatabaseProvider constructor.
     * @param ColumnDTOFactoryInterface $columnDTOFactory
     */
    public function __construct(ColumnDTOFactoryInterface $columnDTOFactory)
    {
        $this->columnDTOFactory = $columnDTOFactory;
    }

    /**
     * @inheritdoc
     */
    public function getColumns(string $boardId): ColumnsDataProviderResultAbstract
    {
        $columns = Column::where('board_id',  $boardId)->get();

        if (!count($columns)) {
            throw new ColumnsNotFoundException();
        }

        $columnsDTO = $this->columnDTOFactory->createCollection(
            $this->transformColumnsToArray($columns)
        );

        return new ColumnsDataProviderDatabaseResult($columnsDTO);
    }

    /**
     * @param Collection $columns
     * @return array
     */
    private function transformColumnsToArray(Collection $columns): array
    {
        $array = [];
        foreach ($columns as $column) {
            $array[] = $this->transformColumnToArray($column);
        }

        return $array;
    }

    /**
     * @param Column $column
     * @return array
     */
    private function transformColumnToArray(Column $column): array
    {
        $columnArray = $column->toArray();
        $columnArray[ColumnDTODatabaseFactory::FIELD_CARDS] = $this->transformCardsToArray($column);
        return $columnArray;
    }

    /**
     * @param Column $column
     * @return array
     */
    private function transformCardsToArray(Column $column): array
    {
        return $column->cards()->get()->toArray();
    }

}