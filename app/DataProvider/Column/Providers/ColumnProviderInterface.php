<?php


namespace App\DataProvider\Column\Providers;


use App\DataProvider\Column\Exception\ColumnsNotFoundException;
use App\DataProvider\Column\Result\ColumnsDataProviderResultAbstract;

interface ColumnProviderInterface
{
    /**
     * @param string $boardId
     * @return ColumnsDataProviderResultAbstract
     * @throws ColumnsNotFoundException
     */
    public function getColumns(string $boardId): ColumnsDataProviderResultAbstract;
}