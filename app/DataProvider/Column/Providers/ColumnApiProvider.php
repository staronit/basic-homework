<?php


namespace App\DataProvider\Column\Providers;


use App\DataProvider\Column\Exception\ColumnsNotFoundException;
use App\DataProvider\Column\Result\ColumnsDataProviderApiResult;
use App\DataProvider\Column\Result\ColumnsDataProviderResultAbstract;
use App\Trello\ColumnApiClientInterface;
use App\Trello\Exception\ApiClientException;

class ColumnApiProvider implements ColumnProviderInterface
{
    /**
     * @var ColumnApiClientInterface
     */
    private $apiClient;

    /**
     * ColumnApiProvider constructor.
     * @param ColumnApiClientInterface $apiClient
     */
    public function __construct(ColumnApiClientInterface $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @inheritdoc
     */
    public function getColumns(string $boardId): ColumnsDataProviderResultAbstract
    {
        try {
            $columnsDTO = $this->apiClient->getColumns($boardId);
        } catch (ApiClientException $exception) {
            throw new ColumnsNotFoundException();
        }

        if (!count($columnsDTO)) {
            throw new ColumnsNotFoundException();
        }

        return new ColumnsDataProviderApiResult($columnsDTO);
    }

}