<?php


namespace App\DataProvider\Column\Result;


use App\DTO\Column\ColumnDTO;

abstract class ColumnsDataProviderResultAbstract
{
    /**
     * @var ColumnDTO[]
     */
    protected $columnsDTO;

    /**
     * ColumnsDataProviderResultAbstract constructor.
     * @param ColumnDTO[] $columnsDTO
     */
    public function __construct(array $columnsDTO)
    {
        $this->columnsDTO = $columnsDTO;
    }


    abstract function sendEvent(): bool;

    /**
     * @return ColumnDTO[]
     */
    public function getColumnsDTO(): array
    {
        return $this->columnsDTO;
    }


}
