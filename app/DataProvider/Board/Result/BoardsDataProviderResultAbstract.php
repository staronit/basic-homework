<?php

namespace App\DataProvider\Board\Result;

use App\DTO\Board\BoardDTO;

abstract class BoardsDataProviderResultAbstract
{
    /**
     * @var BoardDTO[]
     */
    protected $boardDTO;

    /**
     * DataProviderResultAbstract constructor.
     * @param BoardDTO[] $boardDTO
     */
    public function __construct(array $boardDTO)
    {
        $this->boardDTO = $boardDTO;
    }

    abstract function sendEvent(): bool;

    /**
     * @return BoardDTO[]
     */
    public function getBoardDTO(): array
    {
        return $this->boardDTO;
    }
}