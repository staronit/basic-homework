<?php

namespace App\DataProvider\Board\Result;

use App\DTO\Board\BoardDTO;

abstract class BoardDataProviderResultAbstract
{
    /**
     * @var BoardDTO
     */
    protected $boardDTO;

    /**
     * DataProviderResultAbstract constructor.
     * @param BoardDTO $boardDTO
     */
    public function __construct(BoardDTO $boardDTO)
    {
        $this->boardDTO = $boardDTO;
    }

    abstract function sendEvent(): bool;

    /**
     * @return BoardDTO
     */
    public function getBoardDTO(): BoardDTO
    {
        return $this->boardDTO;
    }
}