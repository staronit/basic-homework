<?php

namespace App\DataProvider\Board\Providers;

use App\Board;
use App\DataProvider\Board\Exception\BoardsNotFoundException;
use App\DataProvider\Board\Result\BoardDataProviderDatabaseResult;
use App\DataProvider\Board\Result\BoardDataProviderResultAbstract;
use App\DataProvider\Board\Result\BoardsDataProviderDatabaseResult;
use App\DataProvider\Board\Result\BoardsDataProviderResultAbstract;
use App\DTO\Board\Factory\BoardDTOFactoryInterface;
use App\DTO\Board\Factory\BoardDTODatabaseFactory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BoardDatabaseProvider implements BoardProviderInterface
{
    /**
     * @var BoardDTOFactoryInterface
     */
    private $boardDTOFactory;

    /**
     * BoardDatabaseProvider constructor.
     * @param BoardDTOFactoryInterface $boardDTOFactory
     */
    public function __construct(BoardDTOFactoryInterface $boardDTOFactory)
    {
        $this->boardDTOFactory = $boardDTOFactory;
    }


    /**
     * @return BoardsDataProviderResultAbstract
     * @throws BoardsNotFoundException
     */
    public function getOpenBoards(): BoardsDataProviderResultAbstract
    {
        $boards = Board::all();
        if (!count($boards)) {
            throw new BoardsNotFoundException();
        }
        $boardsDTO = $this->boardDTOFactory->createCollection($this->transformBoardsToArray($boards), []);
        return new BoardsDataProviderDatabaseResult($boardsDTO);
    }

    /**
     * @param string $id
     * @return BoardDataProviderResultAbstract
     * @throws BoardsNotFoundException
     */
    public function getBoard(string $id): BoardDataProviderResultAbstract
    {
        try {
            $board = Board::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            throw new BoardsNotFoundException();
        }

        $boardDTO = $this->boardDTOFactory->create($this->transformBoardToArray($board), []);
        return New BoardDataProviderDatabaseResult($boardDTO);
    }

    /**
     * @param Collection $boards
     * @return array
     */
    private function transformBoardsToArray(Collection $boards)
    {
        $array = [];
        /** @var Board $board */
        foreach ($boards as $board) {
            $array[] = $this->transformBoardToArray($board);
        }

        return $array;
    }

    /**
     * @param Board $board
     * @return array
     */
    private function transformBoardToArray(Board $board)
    {
        $boardArray = $board->toArray();
        $boardArray[BoardDTODatabaseFactory::FIELD_MEMBERSHIP] = $this->transformMembersToArray($board);
        return $boardArray;
    }

    /**
     * @param Board $board
     * @return array
     */
    private function transformMembersToArray(Board $board)
    {
        return $board->users()->get()->toArray();
    }
}