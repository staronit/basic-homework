<?php

namespace App\DataProvider\Board\Providers;

use App\DataProvider\Board\Exception\BoardsNotFoundException;
use App\DataProvider\Board\Result\BoardDataProviderApiResult;
use App\DataProvider\Board\Result\BoardDataProviderResultAbstract;
use App\DataProvider\Board\Result\BoardsDataProviderApiResult;
use App\DataProvider\Board\Result\BoardsDataProviderResultAbstract;
use App\Trello\BoardApiClientInterface;
use App\Trello\Exception\ApiClientException;

class BoardApiProvider implements BoardProviderInterface
{
    /**
     * @var BoardApiClientInterface
     */
    private $apiClient;

    /**
     * BoardApiProvider constructor.
     * @param BoardApiClientInterface $apiClient
     */
    public function __construct(BoardApiClientInterface $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @return BoardsDataProviderResultAbstract
     * @throws BoardsNotFoundException
     */
    public function getOpenBoards(): BoardsDataProviderResultAbstract
    {
        $boardsDTO = $this->apiClient->getOpenBoards();
        if (!$boardsDTO) {
            throw new BoardsNotFoundException();
        }

        return new BoardsDataProviderApiResult($boardsDTO);
    }

    /**
     * @param string $boardId
     * @return BoardDataProviderResultAbstract
     * @throws BoardsNotFoundException
     */
    public function getBoard(string $boardId): BoardDataProviderResultAbstract
    {
        try {
            $boardDTO = $this->apiClient->getBoard($boardId);
        } catch (ApiClientException $exception) {
            throw new BoardsNotFoundException();
        }

        if (!$boardDTO) {
            throw new BoardsNotFoundException();
        }

        return new BoardDataProviderApiResult($boardDTO);

    }


}