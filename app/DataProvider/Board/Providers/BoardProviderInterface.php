<?php

namespace App\DataProvider\Board\Providers;


use App\DataProvider\Board\Result\BoardDataProviderResultAbstract;
use App\DataProvider\Board\Result\BoardsDataProviderResultAbstract;

interface BoardProviderInterface
{
    /**
     * @return BoardsDataProviderResultAbstract
     */
    public function getOpenBoards(): BoardsDataProviderResultAbstract;

    /**
     * @param string $boardId
     * @return BoardDataProviderResultAbstract
     */
    public function getBoard(string $boardId): BoardDataProviderResultAbstract;
}