<?php

namespace App\DataProvider\Board;


use App\DataProvider\Board\Exception\BoardsNotFoundException;
use App\DataProvider\Board\Providers\BoardProviderInterface;
use App\DataProvider\Board\Result\BoardDataProviderResultAbstract;
use App\DataProvider\Board\Result\BoardsDataProviderResultAbstract;

class BoardDataProvider implements BoardDataProviderInterface
{
    /**
     * @var BoardProviderInterface[]
     */
    private $providers;

    /**
     * BoardDataProvider constructor.
     * @param BoardProviderInterface[] $providers
     */
    public function __construct(array $providers)
    {
        $this->providers = $providers;
    }

    /**
     * @return BoardsDataProviderResultAbstract
     * @throws BoardsNotFoundException
     */
    public function getOpenBoards(): BoardsDataProviderResultAbstract
    {
        reset($this->providers);
        foreach ($this->providers as $dataProvider) {
            try {
                return $dataProvider->getOpenBoards();
            } catch (BoardsNotFoundException $exception) { }
        }

        throw new BoardsNotFoundException();
    }

    /**
     * @param string $boardId
     * @return BoardDataProviderResultAbstract
     * @throws BoardsNotFoundException
     */
    public function getBoard(string $boardId): BoardDataProviderResultAbstract
    {
        reset($this->providers);
        foreach ($this->providers as $dataProvider) {
            try {
                return $dataProvider->getBoard($boardId);
            } catch (BoardsNotFoundException $exception) {
            }
        }

        throw new BoardsNotFoundException();
    }


}