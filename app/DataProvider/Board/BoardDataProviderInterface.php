<?php

namespace App\DataProvider\Board;


use App\DataProvider\Board\Result\BoardDataProviderResultAbstract;
use App\DataProvider\Board\Result\BoardsDataProviderResultAbstract;

interface BoardDataProviderInterface
{
    public function getOpenBoards(): BoardsDataProviderResultAbstract;

    public function getBoard(string $boardId): BoardDataProviderResultAbstract;
}