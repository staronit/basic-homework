<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $table = 'boards';

    protected $fillable = [
        'id', 'name',
    ];

    public $incrementing = false;

    public function users()
    {
        return $this->belongsToMany('App\User', 'user_board');
    }
}
