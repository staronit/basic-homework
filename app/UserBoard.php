<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBoard extends Model
{
    protected $table = 'user_board';

    public $timestamps = false;
}
