<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Column extends Model
{
    protected $table = 'columns';

    protected $fillable = [
        'id', 'name',
    ];

    public $incrementing = false;

    public function cards()
    {
        return $this->hasMany('App\Card');
    }
}
