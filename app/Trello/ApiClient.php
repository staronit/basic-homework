<?php

namespace App\Trello;

use App\DTO\Board\BoardDTO;
use App\DTO\Board\Factory\BoardDTOFactoryInterface;
use App\DTO\Column\ColumnDTO;
use App\DTO\Column\Factory\ColumnDTOFactoryInterface;
use App\Trello\Exception\ApiClientException;
use Trello\Api\ApiInterface;
use Trello\Client;
use Trello\Exception\RuntimeException;

class ApiClient implements BoardApiClientInterface, ColumnApiClientInterface
{
    const API_MEMBERS = 'members';
    const API_BOARDS = 'boards';

    const FIELD_MEMBERSHIP = 'memberships';
    const FIELD_ID_MEMBER = 'idMember';

    const KEY_FILTER = 'filter';
    const FILTER_OPEN = 'open';

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $member;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $token;

    /**
     * @var BoardDTOFactoryInterface
     */
    private $boardDTOFactory;

    /**
     * @var ColumnDTOFactoryInterface
     */
    private $columnDTOFactory;

    /**
     * ApiClient constructor.
     * @param Client $client
     * @param string $member
     * @param string $apiKey
     * @param string $token
     * @param BoardDTOFactoryInterface $boardDTOFactory
     * @param ColumnDTOFactoryInterface $columnDTOFactory
     */
    public function __construct(
        Client $client,
        string $member,
        string $apiKey,
        string $token,
        BoardDTOFactoryInterface $boardDTOFactory,
        ColumnDTOFactoryInterface $columnDTOFactory
    )
    {
        $this->client = $client;
        $this->member = $member;
        $this->apiKey = $apiKey;
        $this->token = $token;
        $this->boardDTOFactory = $boardDTOFactory;
        $this->columnDTOFactory = $columnDTOFactory;

        $this->client->authenticate($this->apiKey, $this->token, Client::AUTH_URL_CLIENT_ID);
    }


    /**
     * @inheritdoc
     */
    public function getOpenBoards(): array
    {
        $membersApi = $this->getApi(self::API_MEMBERS);
        try {
            $boards = $membersApi->boards()->all($this->member, [
                self::KEY_FILTER => self::FILTER_OPEN,
                'fields' => 'id,name,memberships',
            ]);
            $members = $this->getBoardsMembers($boards);
        } catch (RuntimeException $exception) {
            throw new ApiClientException(
                $exception->getMessage(),
                $exception->getCode(),
                $exception->getPrevious()
            );
        }

        return $this->boardDTOFactory->createCollection($boards, $members);
    }

    /**
     * @inheritdoc
     */
    public function getBoard(string $boardId): BoardDTO
    {
        $boardsApi = $this->getApi(self::API_BOARDS);
        try {
            $board = $boardsApi->show($boardId, [
                'fields' => 'memberships,name'
            ]);
            $members = $this->getBoardsMembers([
                $board
            ]);
        } catch (RuntimeException $exception) {
            throw new ApiClientException(
                $exception->getMessage(),
                $exception->getCode(),
                $exception->getPrevious()
            );
        }

        return $this->boardDTOFactory->create($board, $members);
    }

    /**
     * @inheritDoc
     */
    public function getColumns(string $boardId): array
    {
        $boardsApi = $this->getApi(self::API_BOARDS);
        try {
            $lists = $boardsApi->lists()->all($boardId, [
                'cards' => 'all',
            ]);
        } catch (RuntimeException $exception) {
            throw new ApiClientException(
                $exception->getMessage(),
                $exception->getCode(),
                $exception->getPrevious()
            );
        }

        return $this->columnDTOFactory->createCollection($lists);
    }


    /**
     * @param array $apiBoards
     * @return array
     */
    private function getBoardsMembers(array $apiBoards)
    {
        $members = [];

        $memberIds = $this->getMembersIdsFromBoards($apiBoards);
        foreach ($memberIds as $memberId) {
            $members[$memberId] = $this->getMemberById($memberId);
        }

        return $members;
    }

    /**
     * @param array $apiBoards
     * @return string[]
     */
    private function getMembersIdsFromBoards(array $apiBoards)
    {
        $memberIds = [];
        foreach ($apiBoards as $apiBoard) {
            $memberIds = array_merge(
                $memberIds,
                $this->getMembersIdsFromBoard($apiBoard)
            );
        }

        return array_unique($memberIds);
    }

    /**
     * @param array $apiBoard
     * @return string[]
     */
    private function getMembersIdsFromBoard(array $apiBoard)
    {
        return array_map(function($apiMember) {
            return $apiMember[self::FIELD_ID_MEMBER];
        }, $apiBoard[self::FIELD_MEMBERSHIP]);
    }

    /**
     * @param string $memberId
     * @return array
     * @throws ApiClientException
     */
    private function getMemberById(string $memberId)
    {
        $memberApi = $this->getApi(self::API_MEMBERS);
        try {
            $member = $memberApi->show($memberId, [
                'fields' => 'id,fullName,email'
            ]);
        } catch (RuntimeException $exception) {
            throw new ApiClientException(
                $exception->getMessage(),
                $exception->getCode(),
                $exception->getPrevious()
            );
        }

        return $member;
    }


    /**
     * @param $name
     * @return ApiInterface
     */
    private function getApi($name): ApiInterface
    {
        return $this->client->api($name);
    }
}