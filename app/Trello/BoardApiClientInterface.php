<?php

namespace App\Trello;

use App\DTO\Board\BoardDTO;

interface BoardApiClientInterface
{
    /**
     * @return BoardDTO[]
     */
    public function getOpenBoards(): array;

    /**
     * @param string $boardId
     * @return BoardDTO
     */
    public function getBoard(string $boardId): BoardDTO;
}