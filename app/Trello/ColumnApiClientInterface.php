<?php


namespace App\Trello;


use App\DTO\Column\ColumnDTO;

interface ColumnApiClientInterface
{
    /**
     * @param string $boardId
     * @return ColumnDTO[]
     */
    public function getColumns(string $boardId): array;
}