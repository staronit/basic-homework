<?php

namespace App\Providers;

use App\DataProvider\Board\BoardDataProviderInterface;
use App\DataProvider\Column\ColumnDataProviderInterface;
use App\Domain\Board\Factory\BoardFactoryInterface;
use App\Domain\Column\Factory\ColumnFactoryInterface;
use App\Repository\Board\BoardReadRepository;
use App\Repository\Board\BoardReadRepositoryInterface;
use App\Repository\Column\ColumnReadRepository;
use App\Repository\Column\ColumnReadRepositoryInterface;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class ReadRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerReadBoardRepository();
        $this->registerReadColumnRepository();
    }

    private function registerReadColumnRepository(): void
    {
        $this->app->bind(
            ColumnReadRepositoryInterface::class,
            function (Application $app) {
                return new ColumnReadRepository(
                    $app->make(ColumnDataProviderInterface::class),
                    $app->make(ColumnFactoryInterface::class)
                );
            }
        );
    }

    private function registerReadBoardRepository(): void
    {
        $this->app->bind(
            BoardReadRepositoryInterface::class,
            function (Application $app) {
                return new BoardReadRepository(
                    $app->make(BoardDataProviderInterface::class),
                    $app->make(BoardFactoryInterface::class)
                );
            }
        );
    }


}
