<?php

namespace App\Providers;

use App\DataProvider\Board\Providers\BoardDatabaseProvider;
use App\DataProvider\Column\Providers\ColumnDatabaseProvider;
use App\DTO\Board\Factory\BoardDTOApiFactory;
use App\DTO\Board\Factory\BoardDTOFactoryInterface;
use App\DTO\Board\Factory\BoardDTODatabaseFactory;
use App\DTO\Card\CardDTOFactoryInterface\CardDTOApiFactory;
use App\DTO\Card\CardDTOFactoryInterface\CardDTODatabaseFactory;
use App\DTO\Card\CardDTOFactoryInterface\CardDTOFactoryInterface;
use App\DTO\Column\Factory\ColumnDTOApiFactory;
use App\DTO\Column\Factory\ColumnDTODatabaseFactory;
use App\DTO\Column\Factory\ColumnDTOFactoryInterface;
use App\DTO\Member\Factory\ApiMemberDTOFactory;
use App\DTO\Member\Factory\DatabaseMemberDTOFactory;
use App\DTO\Member\Factory\MemberDTOFactoryInterface;
use App\Trello\ApiClient;
use Illuminate\Support\ServiceProvider;

class DTOFactoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBoardDTOFactory();
        $this->registerMemberDTOFactory();
        $this->registerCardDTOFactory();
        $this->registerColumnDTOFactory();
    }

    private function registerCardDTOFactory(): void
    {
        $this->app->bind(
            CardDTOFactoryInterface::class,
            CardDTOApiFactory::class
        );

        $this->app
            ->when(ColumnDTOApiFactory::class)
            ->needs(CardDTOFactoryInterface::class)
            ->give(CardDTOApiFactory::class);

        $this->app
            ->when(ColumnDTODatabaseFactory::class)
            ->needs(CardDTOFactoryInterface::class)
            ->give(CardDTODatabaseFactory::class);
    }

    private function registerColumnDTOFactory(): void
    {
        $this->app->bind(
            ColumnDTOFactoryInterface::class,
            ColumnDTOApiFactory::class
        );

        $this->app
            ->when(ColumnDatabaseProvider::class)
            ->needs(ColumnDTOFactoryInterface::class)
            ->give(ColumnDTODatabaseFactory::class);

        $this->app
            ->when(ApiClient::class)
            ->needs(ColumnDTOFactoryInterface::class)
            ->give(ColumnDTOApiFactory::class);
    }

    private function registerMemberDTOFactory(): void
    {
        $this->app->bind(
            MemberDTOFactoryInterface::class,
            ApiMemberDTOFactory::class
        );

        $this->app
            ->when(BoardDTODatabaseFactory::class)
            ->needs(MemberDTOFactoryInterface::class)
            ->give(DatabaseMemberDTOFactory::class);

        $this->app
            ->when(BoardDTOApiFactory::class)
            ->needs(MemberDTOFactoryInterface::class)
            ->give(ApiMemberDTOFactory::class);
    }

    private function registerBoardDTOFactory(): void
    {
        $this->app->bind(
            BoardDTOFactoryInterface::class,
            BoardDTOApiFactory::class
        );

        $this->app
            ->when(BoardDatabaseProvider::class)
            ->needs(BoardDTOFactoryInterface::class)
            ->give(BoardDTODatabaseFactory::class);

        $this->app
            ->when(ApiClient::class)
            ->needs(BoardDTOFactoryInterface::class)
            ->give(BoardDTOApiFactory::class);
    }
}
