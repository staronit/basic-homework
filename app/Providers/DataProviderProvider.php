<?php

namespace App\Providers;

use App\DataProvider\Board\BoardDataProvider;
use App\DataProvider\Board\BoardDataProviderInterface;
use App\DataProvider\Board\Providers\BoardApiProvider;
use App\DataProvider\Board\Providers\BoardDatabaseProvider;
use App\DataProvider\Column\ColumnDataProvider;
use App\DataProvider\Column\ColumnDataProviderInterface;
use App\DataProvider\Column\Providers\ColumnApiProvider;
use App\DataProvider\Column\Providers\ColumnDatabaseProvider;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class DataProviderProvider extends ServiceProvider
{
    const TAG_BOARD = 'board_api_providers';
    const TAG_COLUMN = 'column_api_providers';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBoardDataProviders();
        $this->registerColumnDataProviders();
    }

    private function registerColumnDataProviders(): void
    {
        // bind tag
        $this->app->tag([
            ColumnDatabaseProvider::class,
            ColumnApiProvider::class,
        ], self::TAG_COLUMN);


        // bind provider chain
        $this->app->bind(
            ColumnDataProviderInterface::class,
            function (Application $app) {
                return new ColumnDataProvider(
                    $app->tagged(self::TAG_COLUMN)
                );
            }
        );

    }

    private function registerBoardDataProviders(): void
    {
        // bind tag
        $this->app->tag([
            BoardDatabaseProvider::class,
            BoardApiProvider::class
        ], self::TAG_BOARD);

        // bind provider chain
        $this->app->bind(
            BoardDataProviderInterface::class,
            function (Application $app) {
                return new BoardDataProvider(
                    $app->tagged(self::TAG_BOARD)
                );
            }
        );
    }
}
