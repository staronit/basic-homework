<?php

namespace App\Providers;

use App\Domain\Board\Factory\BoardFactory;
use App\Domain\Board\Factory\BoardFactoryInterface;
use App\Domain\Card\Factory\CardFactory;
use App\Domain\Card\Factory\CardFactoryInterface;
use App\Domain\Column\Factory\ColumnFactory;
use App\Domain\Column\Factory\ColumnFactoryInterface;
use App\Domain\Member\Factory\MemberFactory;
use App\Domain\Member\Factory\MemberFactoryInterface;
use Illuminate\Support\ServiceProvider;

class DomainFactoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerMemberFactory();
        $this->registerBoardFactory();
        $this->registerColumnFactory();
        $this->registerCardFactory();
    }

    private function registerCardFactory(): void
    {
        $this->app->bind(
            CardFactoryInterface::class,
            CardFactory::class
        );
    }

    private function registerColumnFactory(): void
    {
        $this->app->bind(
            ColumnFactoryInterface::class,
            ColumnFactory::class
        );
    }

    private function registerBoardFactory(): void
    {
        $this->app->bind(
            BoardFactoryInterface::class,
            BoardFactory::class
        );
    }

    private function registerMemberFactory(): void {
        $this->app->bind(
            MemberFactoryInterface::class,
            MemberFactory::class
        );
    }
}
