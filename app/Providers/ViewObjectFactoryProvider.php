<?php

namespace App\Providers;

use App\ViewObject\Board\Factory\BoardViewObjectFactory;
use App\ViewObject\Board\Factory\BoardViewObjectFactoryInterface;
use App\ViewObject\Card\Factory\CardViewObjectFactory;
use App\ViewObject\Card\Factory\CardViewObjectFactoryInterface;
use App\ViewObject\Column\Factory\ColumnViewObjectFactory;
use App\ViewObject\Column\Factory\ColumnViewObjectFactoryInterface;
use App\VIewObject\Member\Factory\MemberViewObjectFactory;
use App\VIewObject\Member\Factory\MemberViewObjectFactoryInterface;
use Illuminate\Support\ServiceProvider;

class ViewObjectFactoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerMemberViewObjectFactory();
        $this->registerBoardViewObjectFactory();
        $this->registerCardViewObjectFactory();
        $this->registerColumnViewObjectFactory();
    }

    private function registerCardViewObjectFactory(): void
    {
        $this->app->bind(
            CardViewObjectFactoryInterface::class,
            CardViewObjectFactory::class
        );
    }

    private function registerColumnViewObjectFactory()
    {
        $this->app->bind(
            ColumnViewObjectFactoryInterface::class,
            ColumnViewObjectFactory::class
        );
    }

    private function registerBoardViewObjectFactory(): void
    {
        $this->app->bind(
            BoardViewObjectFactoryInterface::class,
            BoardViewObjectFactory::class
        );
    }

    private function registerMemberViewObjectFactory(): void
    {
        $this->app->bind(
            MemberViewObjectFactoryInterface::class,
            MemberViewObjectFactory::class
        );
    }
}
