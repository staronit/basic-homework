<?php

namespace App\Providers;


use App\ModelFactory\Board\BoardModelModelFactory;
use App\ModelFactory\Board\BoardModelFactoryInterface;
use App\ModelFactory\Card\CardModelFactory;
use App\ModelFactory\Card\CardModelFactoryInterface;
use App\ModelFactory\Column\ColumnModelFactory;
use App\ModelFactory\Column\ColumnModelFactoryInterface;
use App\ModelFactory\User\UserModelModelFactory;
use App\ModelFactory\User\UserModelFactoryInterface;
use Illuminate\Support\ServiceProvider;

class ModelFactoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBoardModelFactory();
        $this->registerUserModelFactory();
        $this->registerCardModelFactory();
        $this->registerColumnModelFactory();
    }

    private function registerCardModelFactory(): void
    {
        $this->app->bind(
            CardModelFactoryInterface::class,
            CardModelFactory::class
        );
    }

    private function registerColumnModelFactory(): void
    {
        $this->app->bind(
            ColumnModelFactoryInterface::class,
            ColumnModelFactory::class
        );
    }


    private function registerUserModelFactory(): void
    {
        $this->app->bind(
            UserModelFactoryInterface::class,
            UserModelModelFactory::class
        );
    }

    private function registerBoardModelFactory(): void
    {
        $this->app->bind(
            BoardModelFactoryInterface::class,
            BoardModelModelFactory::class
        );
    }


}
