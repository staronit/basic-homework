<?php

namespace App\Providers;

use App\Repository\Board\BoardWriteRepository;
use App\Repository\Board\BoardWriteRepositoryInterface;
use App\Repository\Column\ColumnWriteRepository;
use App\Repository\Column\ColumnWriteRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class WriteRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerWriteBoardRepository();
        $this->registerWriteColumnRepository();
    }

    public function registerWriteColumnRepository(): void
    {
        $this->app->bind(
            ColumnWriteRepositoryInterface::class,
            ColumnWriteRepository::class
        );
    }

    private function registerWriteBoardRepository(): void
    {
        $this->app->bind(
            BoardWriteRepositoryInterface::class,
            BoardWriteRepository::class
        );
    }


}
