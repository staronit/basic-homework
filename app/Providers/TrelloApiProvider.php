<?php

namespace App\Providers;

use App\DTO\Board\Factory\BoardDTOFactoryInterface;
use App\DTO\Column\Factory\ColumnDTOFactoryInterface;
use App\Trello\ApiClient;
use App\Trello\BoardApiClientInterface;
use App\Trello\ColumnApiClientInterface;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Trello\Client;

class TrelloApiProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            ColumnApiClientInterface::class,
            function (Application $app) {
                return new ApiClient(
                    $app->make(Client::class),
                    config('services.trello.member'),
                    config('services.trello.apiKey'),
                    config('services.trello.token'),
                    $app->make(BoardDTOFactoryInterface::class),
                    $app->make(ColumnDTOFactoryInterface::class)
                );
            }
        );

        $this->app->singleton(
            BoardApiClientInterface::class,
            function (Application $app) {
                return new ApiClient(
                    $app->make(Client::class),
                    config('services.trello.member'),
                    config('services.trello.apiKey'),
                    config('services.trello.token'),
                    $app->make(BoardDTOFactoryInterface::class),
                    $app->make(ColumnDTOFactoryInterface::class)
                );
            }
        );
    }
}
