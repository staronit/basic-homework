<?php

namespace App\Http\Controllers;

use App\Repository\Board\BoardReadRepositoryInterface;

class BoardController extends Controller
{

    /**
     * @var BoardReadRepositoryInterface
     */
    private $boardReadRepository;

    /**
     * BoardController constructor.
     * @param BoardReadRepositoryInterface $boardReadRepository
     */
    public function __construct(BoardReadRepositoryInterface $boardReadRepository)
    {
        $this->boardReadRepository = $boardReadRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function boards()
    {
        $boards = $this->boardReadRepository->getOpenBoards();
        return view('board.boards', [
            'boards' => $boards
        ]);
    }
}