<?php

namespace App\Http\Controllers;

use App\Domain\Column\Column;
use App\Repository\Column\ColumnReadRepositoryInterface;
use App\Repository\Column\Exception\ColumnNotFoundException;
use App\ViewObject\Column\Factory\ColumnViewObjectFactoryInterface;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class ColumnApiController extends Controller
{
    /**
     * @var ColumnReadRepositoryInterface
     */
    private $columnReadRepository;

    /**
     * @var ColumnViewObjectFactoryInterface
     */
    private $columnViewObjectFactory;

    /**
     * ColumnApiController constructor.
     * @param ColumnReadRepositoryInterface $columnReadRepository
     * @param ColumnViewObjectFactoryInterface $columnViewObjectFactory
     */
    public function __construct(ColumnReadRepositoryInterface $columnReadRepository, ColumnViewObjectFactoryInterface $columnViewObjectFactory)
    {
        $this->columnReadRepository = $columnReadRepository;
        $this->columnViewObjectFactory = $columnViewObjectFactory;
    }


    /**
     * @param string $boardId
     * @return Response
     */
    public function columns(string $boardId): Response
    {
        try {
            $columns = $this->columnReadRepository->getColumns($boardId);
        } catch (ColumnNotFoundException $exception) {
            return $this->responseError('Columns not found', SymfonyResponse::HTTP_NOT_FOUND);
        }
        return $this->responseCollection($columns);
    }


    /**
     * @param string $message
     * @param int $code
     * @return Response
     */
    private function responseError(string $message, int $code): Response
    {
        return response([
            'error' => $message
        ], $code);
    }

    /**
     * @param Column[] $columns
     * @return Response
     */
    private function responseCollection(array $columns): Response
    {
        return $this->response($this->columnViewObjectFactory->createCollection($columns));
    }

    /**
     * @param mixed $data
     * @param int $status
     * @return Response
     */
    private function response($data, int $status = SymfonyResponse::HTTP_OK): Response
    {
        return response($data, $status)->withHeaders([
            'Cache-Control' => 'private, max-age=3600',
        ]);
    }


}