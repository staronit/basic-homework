<?php

namespace App\Http\Controllers;


use App\Domain\Board\Board;
use App\Repository\Board\BoardReadRepositoryInterface;
use App\Repository\Board\Exception\BoardNotFoundException;
use App\ViewObject\Board\Factory\BoardViewObjectFactory;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class BoardApiController extends Controller
{
    /**
     * @var BoardReadRepositoryInterface
     */
    private $boardReadRepository;

    /**
     * @var BoardViewObjectFactory
     */
    private $boardViewObjectFactory;

    /**
     * BoardApiController constructor.
     * @param BoardReadRepositoryInterface $boardReadRepository
     * @param BoardViewObjectFactory $boardViewObjectFactory
     */
    public function __construct(
        BoardReadRepositoryInterface $boardReadRepository,
        BoardViewObjectFactory $boardViewObjectFactory
    )
    {
        $this->boardReadRepository = $boardReadRepository;
        $this->boardViewObjectFactory = $boardViewObjectFactory;
    }

    /**
     * @return Response
     */
    public function boards(): Response
    {
        try {
            $boards = $this->boardReadRepository->getOpenBoards();
        } catch (BoardNotFoundException $exception) {
            return $this->responseError('Boards not found', SymfonyResponse::HTTP_NOT_FOUND);
        }

        return $this->responseCollection($boards);
    }

    /**
     * @param string $message
     * @param int $code
     * @return Response
     */
    private function responseError(string $message, int $code): Response
    {
        return response([
            'error' => $message
        ], $code);
    }

    /**
     * @param Board[] $boards
     * @return Response
     */
    private function responseCollection(array $boards): Response
    {
        return $this->response($this->boardViewObjectFactory->createCollection($boards));
    }

    /**
     * @param mixed $data
     * @param int $status
     * @return Response
     */
    private function response($data, int $status = SymfonyResponse::HTTP_OK): Response
    {
        return response($data, $status)->withHeaders([
            'Cache-Control' => 'private, max-age=3600',
        ]);
    }


}