<?php

namespace App\Http\Controllers;


use App\Repository\Column\ColumnReadRepositoryInterface;
use App\Repository\Column\Exception\ColumnNotFoundException;

class ColumnController extends Controller
{
    /**
     * @var ColumnReadRepositoryInterface
     */
    private $columnRepository;

    /**
     * ColumnController constructor.
     * @param ColumnReadRepositoryInterface $columnRepository
     */
    public function __construct(ColumnReadRepositoryInterface $columnRepository)
    {
        $this->columnRepository = $columnRepository;
    }

    /**
     * @param string $boardId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function columns(string $boardId)
    {
        try {
            $columns = $this->columnRepository->getColumns($boardId);
        } catch (ColumnNotFoundException $exception) {
            abort(404, 'Columns not found');
        }

        return view('columns.columns', [
            'columns' => $columns
        ]);
    }
}