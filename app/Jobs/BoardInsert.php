<?php

namespace App\Jobs;

use App\Domain\Board\Board;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class BoardInsert implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Board[]
     */
    private $boards;

    /**
     * BoardInsert constructor.
     * @param Board[] $boards
     */
    public function __construct(array $boards)
    {
        $this->boards = $boards;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // @todo -> insert $this->board to DB
    }
}
