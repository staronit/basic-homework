<?php

namespace App\VIewObject\Member\Factory;


use App\Domain\Member\Member;
use App\ViewObject\Member\MemberViewObject;

class MemberViewObjectFactory implements MemberViewObjectFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(Member $member): MemberViewObject
    {
        return new MemberViewObject(
            $member->getId(),
            $member->getName(),
            $member->getEmail()
        );
    }

    /**
     * @inheritdoc
     */
    public function createCollection(array $members): array
    {
        $collection = [];
        foreach ($members as $member) {
            $collection[] = $this->create($member);
        }

        return $collection;
    }

}