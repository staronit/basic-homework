<?php

namespace App\VIewObject\Member\Factory;


use App\Domain\Member\Member;
use App\ViewObject\Member\MemberViewObject;

interface MemberViewObjectFactoryInterface
{
    /**
     * @param Member $member
     * @return MemberViewObject
     */
    public function create(Member $member): MemberViewObject;

    /**
     * @param Member[] $members
     * @return MemberViewObject[]
     */
    public function createCollection(array $members): array;
}