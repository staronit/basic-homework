<?php

namespace App\ViewObject\Member;


class MemberViewObject implements \JsonSerializable
{
    const FIELD_ID = 'id';
    const FIELD_NAME = 'name';
    const FIELD_EMAIL = 'email';

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * MemberViewObject constructor.
     * @param string $id
     * @param string $name
     * @param string $email
     */
    public function __construct($id, $name, $email)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
    }


    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            self::FIELD_ID => $this->id,
            self::FIELD_NAME => $this->name,
            self::FIELD_EMAIL => $this->email
        ];
    }


}