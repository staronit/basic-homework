<?php


namespace App\ViewObject\Card\Factory;


use App\Domain\Card\Card;
use App\ViewObject\Card\CardViewObject;

class CardViewObjectFactory implements CardViewObjectFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(Card $card): CardViewObject
    {
        return new CardViewObject(
            $card->getId(),
            $card->getDescription()
        );
    }

    /**
     * @inheritDoc
     */
    public function createCollection(array $cards): array
    {
        $collection = [];
        foreach ($cards as $card) {
            $collection[] = $this->create($card);
        }

        return $collection;
    }

}