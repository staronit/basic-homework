<?php


namespace App\ViewObject\Card\Factory;


use App\Domain\Card\Card;
use App\ViewObject\Card\CardViewObject;

interface CardViewObjectFactoryInterface
{
    /**
     * @param Card $card
     * @return CardViewObject
     */
    public function create(Card $card): CardViewObject;

    /**
     * @param Card[] $cards
     * @return CardViewObject[]
     */
    public function createCollection(array $cards): array;
}