<?php


namespace App\ViewObject\Card;


class CardViewObject implements \JsonSerializable
{
    const FIELD_ID = 'id';
    const FIELD_DESCRIPTION = 'description';

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * CardViewObject constructor.
     * @param string $id
     * @param string $description
     */
    public function __construct($id, $description)
    {
        $this->id = $id;
        $this->description = $description;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            self::FIELD_ID => $this->id,
            self::FIELD_DESCRIPTION => $this->description
        ];
    }


}