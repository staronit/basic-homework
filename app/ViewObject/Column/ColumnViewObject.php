<?php


namespace App\ViewObject\Column;


use App\ViewObject\Card\CardViewObject;

class ColumnViewObject implements \JsonSerializable
{
    const FIELD_ID = 'id';
    const FIELD_NAME = 'name';
    const FIELD_CARDS = 'cards';

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var CardViewObject[]
     */
    private $cards;

    /**
     * ColumnViewObject constructor.
     * @param string $id
     * @param string $name
     * @param CardViewObject[] $cards
     */
    public function __construct($id, $name, array $cards)
    {
        $this->id = $id;
        $this->name = $name;
        $this->cards = $cards;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            self::FIELD_ID => $this->id,
            self::FIELD_NAME => $this->name,
            self::FIELD_CARDS => $this->cards
        ];
    }


}