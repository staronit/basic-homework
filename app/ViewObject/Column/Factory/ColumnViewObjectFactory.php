<?php


namespace App\ViewObject\Column\Factory;


use App\Domain\Column\Column;
use App\ViewObject\Card\Factory\CardViewObjectFactoryInterface;
use App\ViewObject\Column\ColumnViewObject;

class ColumnViewObjectFactory implements ColumnViewObjectFactoryInterface
{
    /**
     * @var CardViewObjectFactoryInterface
     */
    private $cardViewObjectFactory;

    /**
     * ColumnViewObjectFactory constructor.
     * @param CardViewObjectFactoryInterface $cardViewObjectFactory
     */
    public function __construct(CardViewObjectFactoryInterface $cardViewObjectFactory)
    {
        $this->cardViewObjectFactory = $cardViewObjectFactory;
    }

    /**
     * @inheritDoc
     */
    public function create(Column $column): ColumnViewObject
    {
        return new ColumnViewObject(
            $column->getId(),
            $column->getName(),
            $this->cardViewObjectFactory->createCollection($column->getCards())
        );
    }

    /**
     * @inheritDoc
     */
    public function createCollection(array $columns): array
    {
        $collection = [];
        foreach ($columns as $column) {
            $collection[] = $this->create($column);
        }

        return $collection;
    }

}