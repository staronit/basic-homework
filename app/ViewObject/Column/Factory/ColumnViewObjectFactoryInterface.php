<?php


namespace App\ViewObject\Column\Factory;


use App\Domain\Column\Column;
use App\ViewObject\Column\ColumnViewObject;

interface ColumnViewObjectFactoryInterface
{
    /**
     * @param Column $column
     * @return ColumnViewObject
     */
    public function create(Column $column): ColumnViewObject;

    /**
     * @param Column[] $columns
     * @return ColumnViewObject[]
     */
    public function createCollection(array $columns): array;
}