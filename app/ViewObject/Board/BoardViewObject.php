<?php

namespace App\ViewObject\Board;

use App\ViewObject\Member\MemberViewObject;

class BoardViewObject implements \JsonSerializable
{
    const FIELD_ID = 'id';
    const FIELD_NAME = 'name';
    const FIELD_MEMBERS = 'members';

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var MemberViewObject[]
     */
    private $members;

    /**
     * BoardViewObject constructor.
     * @param string $id
     * @param string $name
     * @param MemberViewObject[] $members
     */
    public function __construct($id, $name, array $members)
    {
        $this->id = $id;
        $this->name = $name;
        $this->members = $members;
    }


    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            self::FIELD_ID => $this->id,
            self::FIELD_NAME => $this->name,
            self::FIELD_MEMBERS => $this->members
        ];
    }


}