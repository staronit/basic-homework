<?php

namespace App\ViewObject\Board\Factory;


use App\Domain\Board\Board;
use App\ViewObject\Board\BoardViewObject;
use App\VIewObject\Member\Factory\MemberViewObjectFactoryInterface;

class BoardViewObjectFactory implements BoardViewObjectFactoryInterface
{
    /**
     * @var MemberViewObjectFactoryInterface
     */
    private $memberViewObjectFactory;

    /**
     * BoardViewObjectFactory constructor.
     * @param MemberViewObjectFactoryInterface $memberViewObjectFactory
     */
    public function __construct(MemberViewObjectFactoryInterface $memberViewObjectFactory)
    {
        $this->memberViewObjectFactory = $memberViewObjectFactory;
    }


    /**
     * @inheritdoc
     */
    public function create(Board $board): BoardViewObject
    {
        return new BoardViewObject(
            $board->getId(),
            $board->getName(),
            $this->memberViewObjectFactory->createCollection($board->getMembers())
        );
    }

    /**
     * @inheritdoc
     */
    public function createCollection(array $boards): array
    {
       $collection = [];
       foreach ($boards as $board) {
           $collection[] = $this->create($board);
       }

       return $collection;
    }

}