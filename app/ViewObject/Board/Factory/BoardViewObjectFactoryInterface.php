<?php

namespace App\ViewObject\Board\Factory;


use App\Domain\Board\Board;
use App\ViewObject\Board\BoardViewObject;

interface BoardViewObjectFactoryInterface
{
    /**
     * @param Board $board
     * @return BoardViewObject
     */
    public function create(Board $board): BoardViewObject;

    /**
     * @param Board[] $boards
     * @return BoardViewObject[]
     */
    public function createCollection(array $boards): array;
}