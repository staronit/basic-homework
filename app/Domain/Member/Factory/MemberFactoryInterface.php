<?php
namespace App\Domain\Member\Factory;


use App\Domain\Member\Member;
use App\DTO\Member\MemberDTO;

interface MemberFactoryInterface
{

    /**
     * @param MemberDTO $member
     * @return Member
     */
    public function create(MemberDTO $member): Member;

    /**
     * @param MemberDTO[] $data
     * @return Member[]
     */
    public function createCollection(array $data): array;
}