<?php

namespace App\Domain\Member\Factory;


use App\Domain\Member\Member;
use App\DTO\Member\MemberDTO;

class MemberFactory implements MemberFactoryInterface
{
    /**
     * @inheritdoc
     */
    public function create(MemberDTO $member): Member
    {
        return new Member(
            $member->getId(),
            $member->getName(),
            $member->getEmail()
        );
    }

    /**
     * @inheritdoc
     */
    public function createCollection(array $data): array
    {
        $members = [];
        foreach ($data as $memberDTO) {
            $members[] = $this->create($memberDTO);
        }

        return $members;
    }


}