<?php

namespace App\Domain\Board;

use App\Domain\Member\Member;

class Board
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Member[]
     */
    private $members;

    /**
     * Board constructor.
     * @param string $id
     * @param string $name
     * @param Member[] $members
     */
    public function __construct($id, $name, array $members)
    {
        $this->id = $id;
        $this->name = $name;
        $this->members = $members;
    }


    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Member[]
     */
    public function getMembers(): array
    {
        return $this->members;
    }
}