<?php

namespace App\Domain\Board\Factory;


use App\Domain\Board\Board;
use App\Domain\Member\Factory\MemberFactoryInterface;
use App\DTO\Board\BoardDTO;

class BoardFactory implements BoardFactoryInterface
{
    /**
     * @var MemberFactoryInterface
     */
    private $memberFactory;

    /**
     * BoardFactory constructor.
     * @param MemberFactoryInterface $memberFactory
     */
    public function __construct(MemberFactoryInterface $memberFactory)
    {
        $this->memberFactory = $memberFactory;
    }

    /**
     * @inheritdoc
     */
    public function create(BoardDTO $board): Board
    {
        return new Board(
            $board->getId(),
            $board->getName(),
            $this->memberFactory->createCollection($board->getMembers())
        );
    }

    /**
     * @inheritdoc
     */
    public function createCollection(array $data): array
    {
        $boards = [];
        foreach ($data as $boardData) {
            $boards[] = $this->create($boardData);
        }

        return $boards;
    }

}