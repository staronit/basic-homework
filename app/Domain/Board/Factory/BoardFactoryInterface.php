<?php
/**
 * Created by PhpStorm.
 * User: jacek
 * Date: 26.11.17
 * Time: 17:40
 */

namespace App\Domain\Board\Factory;


use App\Domain\Board\Board;
use App\DTO\Board\BoardDTO;

interface BoardFactoryInterface
{
    /**
     * @param BoardDTO $board
     * @return Board
     */
    public function create(BoardDTO $board): Board;

    /**
     * @param BoardDTO[] $data
     * @return Board[]
     */
    public function createCollection(array $data): array;
}