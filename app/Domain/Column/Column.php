<?php

namespace App\Domain\Column;

use App\Domain\Card\Card;

class Column
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $boardId;

    /**
     * @var Card[]
     */
    private $cards;

    /**
     * Column constructor.
     * @param string $id
     * @param string $name
     * @param string $boardId
     * @param Card[] $cards
     */
    public function __construct($id, $name, $boardId, array $cards)
    {
        $this->id = $id;
        $this->name = $name;
        $this->boardId = $boardId;
        $this->cards = $cards;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getBoardId(): string
    {
        return $this->boardId;
    }

    /**
     * @return Card[]
     */
    public function getCards(): array
    {
        return $this->cards;
    }

    /**
     * @return int
     */
    public function getCardsCount(): int
    {
        return count($this->cards);
    }
}