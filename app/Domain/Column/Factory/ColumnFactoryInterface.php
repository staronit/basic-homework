<?php


namespace App\Domain\Column\Factory;

use App\Domain\Column\Column;
use App\DTO\Column\ColumnDTO;

interface ColumnFactoryInterface
{
    /**
     * @param ColumnDTO $columnDTO
     * @return Column
     */
    public function create(ColumnDTO $columnDTO): Column;

    /**
     * @param ColumnDTO[] $columnsDTO
     * @return Column[]
     */
    public function createCollection(array $columnsDTO): array;
}