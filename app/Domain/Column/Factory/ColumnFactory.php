<?php


namespace App\Domain\Column\Factory;


use App\Domain\Card\Factory\CardFactoryInterface;
use App\Domain\Column\Column;
use App\DTO\Column\ColumnDTO;

class ColumnFactory implements ColumnFactoryInterface
{
    /**
     * @var CardFactoryInterface
     */
    private $cardFactory;

    /**
     * ColumnFactory constructor.
     * @param CardFactoryInterface $cardFactory
     */
    public function __construct(CardFactoryInterface $cardFactory)
    {
        $this->cardFactory = $cardFactory;
    }


    /**
     * @inheritDoc
     */
    public function create(ColumnDTO $columnDTO): Column
    {
        return new Column(
            $columnDTO->getId(),
            $columnDTO->getName(),
            $columnDTO->getBoardId(),
            $this->cardFactory->createCollection($columnDTO->getCards())
        );
    }

    /**
     * @inheritDoc
     */
    public function createCollection(array $columnsDTO): array
    {
        $columns = [];
        foreach ($columnsDTO as $columnDTO) {
            $columns[] = $this->create($columnDTO);
        }

        return $columns;
    }

}