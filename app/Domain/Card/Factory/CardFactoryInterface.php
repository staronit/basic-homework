<?php

namespace App\Domain\Card\Factory;

use App\Domain\Card\Card;
use App\DTO\Card\CardDTO;

interface CardFactoryInterface
{
    /**
     * @param CardDTO $cardDTO
     * @return Card
     */
    public function create(CardDTO $cardDTO): Card;

    /**
     * @param CardDTO[] $cardsDTO
     * @return Card[]
     */
    public function createCollection(array $cardsDTO): array;
}