<?php


namespace App\Domain\Card\Factory;


use App\Domain\Card\Card;
use App\DTO\Card\CardDTO;

class CardFactory implements CardFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(CardDTO $cardDTO): Card
    {
        return new Card(
            $cardDTO->getId(),
            $cardDTO->getDescription()
        );
    }

    /**
     * @inheritDoc
     */
    public function createCollection(array $cardsDTO): array
    {
        $cards = [];
        foreach ($cardsDTO as $cardDTO) {
            $cards[] = $this->create($cardDTO);
        }

        return $cards;
    }
}