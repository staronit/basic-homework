<?php

namespace App\Events;

use App\Domain\Board\Board;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class BoardsReceivedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Board[]
     */
    private $boards;

    /**
     * BoardReceivedEvent constructor.
     * @param Board[] $boards
     */
    public function __construct(array $boards)
    {
        $this->boards = $boards;
    }


    /**
     * @return Board[]
     */
    public function getBoards(): array
    {
        return $this->boards;
    }
}
