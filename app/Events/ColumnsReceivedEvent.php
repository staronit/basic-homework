<?php

namespace App\Events;

use App\Domain\Column\Column;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class ColumnsReceivedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Column[]
     */
    private $columns;

    /**
     * ColumnsReceivedEvent constructor.
     * @param Column[] $columns
     */
    public function __construct(array $columns)
    {
        $this->columns = $columns;
    }

    /**
     * @return Column[]
     */
    public function getColumns(): array
    {
        return $this->columns;
    }

}
