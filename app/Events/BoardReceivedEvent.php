<?php

namespace App\Events;

use App\Domain\Board\Board;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class BoardReceivedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Board
     */
    private $board;

    /**
     * BoardReceivedEvent constructor.
     * @param Board $board
     */
    public function __construct(Board $board)
    {
        $this->board = $board;
    }


    /**
     * @return Board
     */
    public function getBoard(): Board
    {
        return $this->board;
    }
}
