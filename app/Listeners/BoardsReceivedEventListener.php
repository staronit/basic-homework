<?php

namespace App\Listeners;

use App\Events\BoardsReceivedEvent;
use App\Repository\Board\BoardWriteRepositoryInterface;

class BoardsReceivedEventListener
{
    /**
     * @var BoardWriteRepositoryInterface
     */
    private $boardWriteRepository;

    /**
     * BoardsReceivedEventListener constructor.
     * @param BoardWriteRepositoryInterface $boardWriteRepository
     */
    public function __construct(BoardWriteRepositoryInterface $boardWriteRepository)
    {
        $this->boardWriteRepository = $boardWriteRepository;
    }

    /**
     * Handle the event.
     *
     * @todo add to queue
     *
     * @param  BoardsReceivedEvent  $event
     * @return void
     */
    public function handle(BoardsReceivedEvent $event)
    {
        $this->boardWriteRepository->saveBoards($event->getBoards());
    }
}
