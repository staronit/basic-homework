<?php


namespace App\Listeners;


use App\Events\ColumnsReceivedEvent;
use App\Repository\Column\ColumnWriteRepositoryInterface;

class ColumnsReceivedEventListener
{

    /**
     * @var ColumnWriteRepositoryInterface
     */
    private $columnWriteRepository;

    /**
     * ColumnsReceivedEventListener constructor.
     * @param ColumnWriteRepositoryInterface $columnWriteRepository
     */
    public function __construct(ColumnWriteRepositoryInterface $columnWriteRepository)
    {
        $this->columnWriteRepository = $columnWriteRepository;
    }

    /**
     * Handle the event.
     *
     * @todo add to queue
     *
     * @param  ColumnsReceivedEvent  $event
     * @return void
     */
    public function handle(ColumnsReceivedEvent $event)
    {
        $this->columnWriteRepository->saveColumns($event->getColumns());
    }
}