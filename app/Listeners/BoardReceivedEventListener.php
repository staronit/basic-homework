<?php

namespace App\Listeners;

use App\Events\BoardReceivedEvent;
use App\Repository\Board\BoardWriteRepositoryInterface;

class BoardReceivedEventListener
{
    /**
     * @var BoardWriteRepositoryInterface
     */
    private $boardWriteRepository;

    /**
     * BoardReceivedEventListener constructor.
     * @param BoardWriteRepositoryInterface $boardWriteRepository
     */
    public function __construct(BoardWriteRepositoryInterface $boardWriteRepository)
    {
        $this->boardWriteRepository = $boardWriteRepository;
    }

    /**
     * Handle the event.
     *
     * @todo add to queue
     *
     * @param  BoardReceivedEvent  $event
     * @return void
     */
    public function handle(BoardReceivedEvent $event)
    {
        $this->boardWriteRepository->saveBoard($event->getBoard());
    }
}
