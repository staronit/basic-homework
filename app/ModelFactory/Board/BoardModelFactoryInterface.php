<?php

namespace App\ModelFactory\Board;

use App\Board;
use App\Domain\Board\Board as BoardDomain;

interface BoardModelFactoryInterface
{
    /**
     * @param BoardDomain $board
     * @return Board
     */
    public function create(BoardDomain $board): Board;
}