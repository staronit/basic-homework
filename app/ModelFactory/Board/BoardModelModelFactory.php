<?php

namespace App\ModelFactory\Board;

use App\Board;
use App\Domain\Board\Board as BoardDomain;

class BoardModelModelFactory implements BoardModelFactoryInterface
{

    /**
     * @inheritDoc
     */
    public function create(BoardDomain $board): Board
    {
        $boardModel = Board::firstOrNew([
            'id' => $board->getId()
        ]);

        $boardModel->name = $board->getName();

        return $boardModel;
    }
}