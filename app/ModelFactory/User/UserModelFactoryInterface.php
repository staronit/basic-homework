<?php

namespace App\ModelFactory\User;

use App\Domain\Member\Member as DomainMember;
use App\User;

interface UserModelFactoryInterface
{
    /**
     * @param DomainMember $member
     * @return User
     */
    public function create(DomainMember $member): User;

    /**
     * @param DomainMember[] $members
     * @return User[]
     */
    public function createCollection(array $members): array;
}