<?php

namespace App\ModelFactory\User;


use App\Domain\Member\Member as DomainMember;
use App\User;

class UserModelModelFactory implements UserModelFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(DomainMember $member): User
    {
        $userModel = User::firstOrNew([
            'id' => $member->getId()
        ]);

        $userModel->name = $member->getName();
        $userModel->email = $member->getEmail();

        return $userModel;
    }

    /**
     * @inheritDoc
     */
    public function createCollection(array $members): array
    {
        $collection = [];
        foreach ($members as $member) {
            $collection[] = $this->create($member);
        }

        return $collection;
    }


}