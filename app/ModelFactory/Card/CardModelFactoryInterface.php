<?php


namespace App\ModelFactory\Card;

use App\Card;
use App\Domain\Card\Card as CardDomain;


interface CardModelFactoryInterface
{
    /**
     * @param CardDomain $card
     * @return Card
     */
    public function create(CardDomain $card): Card;

    /**
     * @param CardDomain[] $cards
     * @return Card[]
     */
    public function createCollection(array $cards): array;
}