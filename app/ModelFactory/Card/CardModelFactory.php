<?php


namespace App\ModelFactory\Card;


use App\Card;
use App\Domain\Card\Card as CardDomain;

class CardModelFactory implements CardModelFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(CardDomain $card): Card
    {
        $cardModel = Card::firstOrNew([
            'id' => $card->getId()
        ]);

        $cardModel->name = $card->getDescription();

        return $cardModel;
    }

    /**
     * @inheritDoc
     */
    public function createCollection(array $cards): array
    {
        $collection = [];
        foreach ($cards as $card) {
            $collection[] = $this->create($card);
        }

        return $collection;
    }
}