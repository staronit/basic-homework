<?php


namespace App\ModelFactory\Column;

use App\Column;
use App\Domain\Column\Column as ColumnDomain;

interface ColumnModelFactoryInterface
{
    /**
     * @param ColumnDomain $column
     * @return Column
     */
    public function create(ColumnDomain $column): Column;

    /**
     * @param ColumnDomain[] $columns
     * @return Column[]
     */
    public function createCollection(array $columns): array;
}