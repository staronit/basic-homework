<?php


namespace App\ModelFactory\Column;


use App\Column;
use App\Domain\Column\Column as ColumnDomain;

class ColumnModelFactory implements ColumnModelFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(ColumnDomain $column): Column
    {
        /** @var Column $columnModel */
        $columnModel = Column::firstOrNew([
            'id' => $column->getId()
        ]);

        $columnModel->name = $column->getName();
        $columnModel->board_id = $column->getBoardId();

        return $columnModel;
    }

    /**
     * @inheritDoc
     */
    public function createCollection(array $columns): array
    {
        $collection = [];
        foreach ($columns as $column) {
            $collection[] = $this->create($column);
        }

        return $collection;
    }
}