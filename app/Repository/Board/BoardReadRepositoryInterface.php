<?php

namespace App\Repository\Board;

use App\Domain\Board\Board;

interface BoardReadRepositoryInterface
{
    /**
     * @return Board[]
     */
    public function getOpenBoards(): array;

    /**
     * @param string $boardId
     * @return Board
     */
    public function getBoard(string $boardId): Board;
}