<?php

namespace App\Repository\Board;

use App\Board as BoardModel;
use App\ModelFactory\Board\BoardModelFactoryInterface;
use App\ModelFactory\User\UserModelFactoryInterface;
use App\User as UserModel;
use App\Domain\Board\Board;

class BoardWriteRepository implements BoardWriteRepositoryInterface
{
    /**
     * @var BoardModelFactoryInterface
     */
    private $boardModelFactory;

    /**
     * @var UserModelFactoryInterface
     */
    private $userModelFactory;

    /**
     * BoardWriteRepository constructor.
     * @param BoardModelFactoryInterface $boardModelFactory
     * @param UserModelFactoryInterface $userModelFactory
     */
    public function __construct(
        BoardModelFactoryInterface $boardModelFactory,
        UserModelFactoryInterface $userModelFactory
    )
    {
        $this->boardModelFactory = $boardModelFactory;
        $this->userModelFactory = $userModelFactory;
    }


    /**
     * @inheritDoc
     */
    public function saveBoard(Board $board): void
    {
        \DB::beginTransaction();

        $this->saveSingleBoard($board);

        \DB::commit();
    }

    /**
     * @inheritDoc
     */
    public function saveBoards(array $boards): void
    {
        \DB::beginTransaction();

        foreach ($boards as $board) {
            $this->saveSingleBoard($board);
        }

        \DB::commit();
    }

    /**
     * @param Board $board
     */
    private function saveSingleBoard(Board $board): void
    {
        $boardModel = $this->boardModelFactory->create($board);
        if (!$boardModel->exists) {
            $boardModel->saveOrFail();
        }

        $users = $this->userModelFactory->createCollection($board->getMembers());
        foreach ($users as $user) {
            if (!$user->exists) {
                $user->saveOrFail();
            }
        }

        $this->attachUsers($boardModel, $users);
    }


    /**
     * @param BoardModel $boardModel
     * @param UserModel[] $users
     */
    private function attachUsers(BoardModel $boardModel, array $users): void
    {
        $userIds = [];
        foreach ($users as $user) {
            $userIds[] = $user->id;
        }

        $boardModel->users()->sync($userIds);
    }


}