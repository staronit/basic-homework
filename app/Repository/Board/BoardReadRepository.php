<?php

namespace App\Repository\Board;

use App\DataProvider\Board\BoardDataProviderInterface;
use App\DataProvider\Board\Exception\BoardsNotFoundException;
use App\Domain\Board\Board;
use App\Domain\Board\Factory\BoardFactoryInterface;
use App\Events\BoardReceivedEvent;
use App\Events\BoardsReceivedEvent;
use App\Repository\Board\Exception\BoardNotFoundException;

class BoardReadRepository implements BoardReadRepositoryInterface
{
    /**
     * @var BoardDataProviderInterface
     */
    private $boardDataProvider;

    /**
     * @var BoardFactoryInterface
     */
    private $boardFactory;

    /**
     * BoardReadRepository constructor.
     * @param BoardDataProviderInterface $boardDataProvider
     * @param BoardFactoryInterface $boardFactory
     */
    public function __construct(
        BoardDataProviderInterface $boardDataProvider,
        BoardFactoryInterface $boardFactory
    )
    {
        $this->boardDataProvider = $boardDataProvider;
        $this->boardFactory = $boardFactory;
    }


    /**
     * @inheritdoc
     */
    public function getOpenBoards(): array
    {
        try {
            $dataProviderResult = $this->boardDataProvider->getOpenBoards();
        } catch (BoardsNotFoundException $exception) {
            throw new BoardNotFoundException();
        }

        $boards = $this->boardFactory->createCollection($dataProviderResult->getBoardDTO());
        if ($dataProviderResult->sendEvent()) {
            event(new BoardsReceivedEvent($boards));
        }

        return $boards;
    }

    /**
     * @inheritdoc
     */
    public function getBoard(string $boardId): Board
    {
        try {
            $dataProviderResult = $this->boardDataProvider->getBoard($boardId);
        } catch (BoardsNotFoundException $exception) {
            throw new BoardNotFoundException();
        }

        $board = $this->boardFactory->create($dataProviderResult->getBoardDTO());
        if ($dataProviderResult->sendEvent()) {
            event(new BoardReceivedEvent($board));
        }

        return $board;
    }


}