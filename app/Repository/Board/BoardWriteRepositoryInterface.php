<?php

namespace App\Repository\Board;


use App\Domain\Board\Board;

interface BoardWriteRepositoryInterface
{
    /**
     * @param Board $board
     */
    public function saveBoard(Board $board): void;

    /**
     * @param Board[] $boards
     */
    public function saveBoards(array $boards): void;
}