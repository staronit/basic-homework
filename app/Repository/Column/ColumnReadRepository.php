<?php


namespace App\Repository\Column;


use App\DataProvider\Column\ColumnDataProviderInterface;
use App\DataProvider\Column\Exception\ColumnsNotFoundException;
use App\Domain\Column\Factory\ColumnFactoryInterface;
use App\Events\ColumnsReceivedEvent;
use App\Repository\Column\Exception\ColumnNotFoundException;

class ColumnReadRepository implements ColumnReadRepositoryInterface
{

    /**
     * @var ColumnDataProviderInterface
     */
    private $columnDataProvider;

    /**
     * @var ColumnFactoryInterface
     */
    private $columnFactory;

    /**
     * ColumnReadRepository constructor.
     * @param ColumnDataProviderInterface $columnDataProvider
     * @param ColumnFactoryInterface $columnFactory
     */
    public function __construct(
        ColumnDataProviderInterface $columnDataProvider,
        ColumnFactoryInterface $columnFactory)
    {
        $this->columnDataProvider = $columnDataProvider;
        $this->columnFactory = $columnFactory;
    }

    /**
     * @inheritDoc
     */
    public function getColumns(string $boardId): array
    {
        try {
            $dataProviderResult = $this->columnDataProvider->getColumns($boardId);
        } catch (ColumnsNotFoundException $exception) {
            throw new ColumnNotFoundException();
        }

        $columns = $this->columnFactory->createCollection($dataProviderResult->getColumnsDTO());
        if ($dataProviderResult->sendEvent()) {
            event(new ColumnsReceivedEvent($columns));
        }

        return $columns;
    }


}