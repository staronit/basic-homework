<?php

namespace App\Repository\Column;


use App\Domain\Column\Column;

interface ColumnReadRepositoryInterface
{
    /**
     * @param string $boardId
     * @return Column[]
     */
    public function getColumns(string $boardId): array;
}