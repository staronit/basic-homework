<?php


namespace App\Repository\Column;


use App\Domain\Column\Column;

interface ColumnWriteRepositoryInterface
{
    /**
     * @param Column[] $columns
     */
    public function saveColumns(array $columns): void;
}