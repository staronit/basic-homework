<?php


namespace App\Repository\Column;


use App\Domain\Column\Column;
use App\ModelFactory\Card\CardModelFactoryInterface;
use App\ModelFactory\Column\ColumnModelFactoryInterface;

class ColumnWriteRepository implements ColumnWriteRepositoryInterface
{
    /**
     * @var CardModelFactoryInterface
     */
    private $cardModelFactory;

    /**
     * @var ColumnModelFactoryInterface
     */
    private $columnModelFactory;

    /**
     * ColumnWriteRepository constructor.
     * @param CardModelFactoryInterface $cardModelFactory
     * @param ColumnModelFactoryInterface $columnModelFactory
     */
    public function __construct(
        CardModelFactoryInterface $cardModelFactory,
        ColumnModelFactoryInterface $columnModelFactory
    )
    {
        $this->cardModelFactory = $cardModelFactory;
        $this->columnModelFactory = $columnModelFactory;
    }


    /**
     * @inheritDoc
     */
    public function saveColumns(array $columns): void
    {
        \DB::beginTransaction();
        foreach ($columns as $column) {
            $this->saveSingleColumn($column);
        }
        \DB::commit();
    }

    private function saveSingleColumn(Column $column): void
    {
        $columnModel = $this->columnModelFactory->create($column);
        $columnModel->saveOrFail();

        $columnModel->cards()->saveMany(
            $this->cardModelFactory->createCollection($column->getCards())
        );
    }
}